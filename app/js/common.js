var Site = {
    $some:null,
    onload: document.addEventListener('DOMContentLoaded', function () {
        Site.init();
        Site.resize();
        Site.scroll();
    }),
    init: function () {
        this.langSwitch();
        this.loginSearch();
        this.loginForgot();
        this.sidebar();
        this.sidebarCheck();
        this.headerSearch();
        this.sticky();
        this.datepickers();
        this.modal();
        this.charts();
        this.multiselect();
        this.dataTables();
        if($('#calendar').is(':visible')){
            this.calendar();
        }
        this.editor();
        this.vectorMap();
        this.notifications();
    },
    resize: function(){
        $(window).resize(function () {
            Site.sidebarCheck();
        });
    },
    scroll: function(){
        $(window).scroll(function () {
            Site.sticky();
        });
    },
    langSwitch: function () {
        $(document).on('click', '.login-lang-switch', function () {
            $('.login-lang-list').toggleClass('opened');
        });
    },
    loginSearch:function () {
        $(document).on('click', '.login-search-btn', function () {
            $('.login-search').addClass('opened');
        }).on('click', '.login-search-close', function () {
            $('.login-search').removeClass('opened');
        });
    },
    loginForgot:function () {
        $(document).on('click', '.login-form-buttons-link_forgot', function () {
            $('.signIn').addClass('hidden');
            $('.forgot').removeClass('hidden');
        }).on('click', '.login-form-buttons-button_back', function () {
            $('.forgot').addClass('hidden');
            $('.signIn').removeClass('hidden');
        });
    },
    sidebar:function () {
        $(document).on('click', '.sidebarToggler, .sidebar-overlay', function () {
            if($('.sidebar').is('.short')){
                Site.sidebarFull();
            } else{
                Site.sidebarShort();
            }
        }).on('click', '.sidebar-account-name, .sidebar-account-image', function () {
            $('.sidebar-account').toggleClass('opened');
        });

    },
    sidebarShort: function(){
        $('.sidebar').addClass('short');
        $('.sidebarToggler').addClass('opened');
        $('.content').addClass('full');
        $('.header').addClass('full');
    },
    sidebarFull: function(){
        $('.sidebar').removeClass('short');
        $('.sidebarToggler').removeClass('opened');
        $('.content').removeClass('full');
        $('.header').removeClass('full');
    },
    sidebarCheck:function(){
        if($(window).width()<=1024){
            Site.sidebarShort();
        } else{
            Site.sidebarFull();
        }
    },
    headerSearch: function () {
        $(document).on('click', '.header-search-button', function () {
            $('.header-search-wrap').addClass('opened');
            $('.header').addClass('searchOpened');
        }).on('click', '.header-search-close, .sidebar-account-image', function () {
            $('.header-search-wrap').removeClass('opened');
            $('.header').removeClass('searchOpened');
        });
    },
    sticky: function () {
        if ($(window).scrollTop() > 0){
            $('.header').addClass('sticky');
        } else {
            $('.header').removeClass('sticky');
        }
    },
    datepickers: function () {
        $('#content-upload-date').datepicker({
            language: 'en',
            autoClose:true,
            dateFormat: 'M dd, yyyy',
            minDate: new Date(),
            range: true,
            multipleDatesSeparator: " - "
        });
    },
    charts:function () {
        if($('#viewsChart').is(':visible')) {
            var viewsChart_ctx = document.getElementById("viewsChart").getContext('2d');
            var viewsChart = new Chart(viewsChart_ctx, {
                type: 'line',
                data: {
                    labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul'],
                    datasets: [
                        {
                            label: 'Data',
                            backgroundColor: 'rgba(19, 170, 249, 0.5)',
                            borderColor: '#13aaf9',
                            data: [3000, 10000, 7500, 7000, 3000, 7000, 3000],
                            borderWidth: 2
                        },
                        {
                            label: 'Data2',
                            backgroundColor: 'rgba(231, 165, 128, 0.5)',
                            borderColor: '#e7a580',
                            data: [0, 3000, 7500, 10000, 1000, 8000, 7500],
                            borderWidth: 2
                        }
                    ]
                },
                options: {
                    responsive: true,
                    legend: {
                        display: false
                    },
                    title: {
                        display: false
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false,
                    },
                    hover: {
                        mode: 'nearest',
                        intersect: true
                    },
                    scales: {
                        xAxes: [{
                            display: true,
                            scaleLabel: {
                                display: false
                            }
                        }],
                        yAxes: [{
                            display: true,
                            scaleLabel: {
                                display: false
                            }
                        }]
                    }
                }

            });
        }
        if($('#ContentUploadedChart').is(':visible')) {
            var contentUploadedChart_ctx = document.getElementById("ContentUploadedChart").getContext('2d');
            var contentUploadedChart = new Chart(contentUploadedChart_ctx, {
                type: 'horizontalBar',
                data: {
                    labels: ['Johny Ive', 'Rick Summers', 'Entony Grass', 'Angela Goldensimth', 'Valerian Voltec'],
                    datasets: [
                        {
                            label: 'Data',
                            backgroundColor: 'rgba(19, 170, 249, 0.5)',
                            borderColor: '#13aaf9',
                            data: [100, 80, 60, 45, 59],
                            borderWidth: 2
                        },
                        {
                            label: 'Data2',
                            backgroundColor: 'rgba(231, 165, 128, 0.5)',
                            borderColor: '#e7a580',
                            data: [45, 50, 30, 75, 10],
                            borderWidth: 2
                        }
                    ]
                },
                options: {
                    responsive: true,
                    legend: {
                        display: false
                    },
                    title: {
                        display: false
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false,
                    },
                    hover: {
                        mode: 'nearest',
                        intersect: true
                    },
                    scales: {
                        xAxes: [{
                            display: true,
                            scaleLabel: {
                                display: false
                            }
                        }],
                        yAxes: [{
                            display: true,
                            scaleLabel: {
                                display: false
                            }
                        }]
                    }
                }

            })
        }
        if($('#documentViews').is(':visible')) {
            var documentViews_ctx = document.getElementById("documentViews").getContext('2d');
            var documentViews = new Chart(documentViews_ctx, {
                type: 'line',
                data: {
                    labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul'],
                    datasets: [
                        {
                            label: 'Data',
                            backgroundColor: 'transparent',
                            borderColor: '#ff6888',
                            data: [3000, 10000, 7500, 7000, 3000, 7000, 3000],
                            borderWidth: 2
                        },
                        {
                            label: 'Data2',
                            backgroundColor: 'transparent',
                            borderColor: '#289cea',
                            data: [0, 3000, 7500, 10000, 1000, 8000, 7500],
                            borderWidth: 2
                        }
                    ]
                },
                options: {
                    responsive: true,
                    legend: {
                        display: false
                    },
                    title: {
                        display: false
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false,
                    },
                    hover: {
                        mode: 'nearest',
                        intersect: true
                    },
                    scales: {
                        xAxes: [{
                            display: true,
                            scaleLabel: {
                                display: false
                            }
                        }],
                        yAxes: [{
                            display: true,
                            scaleLabel: {
                                display: false
                            }
                        }]
                    }
                }

            });
        }
        if($('#yearlyDownloads').is(':visible')) {
            var yearlyDownloads_ctx = document.getElementById("yearlyDownloads").getContext('2d');
            var yearlyDownloads = new Chart(yearlyDownloads_ctx, {
                type: 'line',
                data: {
                    labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul'],
                    datasets: [
                        {
                            label: 'Data',
                            backgroundColor: 'transparent',
                            borderColor: '#289cea',
                            data: [-100, 65, 100, 90, 10, -20, -60],
                            borderWidth: 2
                        },
                        {
                            label: 'Data2',
                            backgroundColor: 'transparent',
                            borderColor: '#40bcbc',
                            data: [40, -80, -40, 40, -30, -40, 100],
                            borderWidth: 2,
                            borderDash: [5,5]
                        },
                        {
                            label: 'Data3',
                            backgroundColor: '#ff6384',
                            borderColor: '#ff6384',
                            data: [50, 90, 85, 30, 10, -90, -10],
                            borderWidth: 2,
                        }
                    ]
                },
                options: {
                    responsive: true,
                    legend: {
                        display: false
                    },
                    title: {
                        display: false
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false,
                    },
                    hover: {
                        mode: 'nearest',
                        intersect: true
                    },
                    scales: {
                        xAxes: [{
                            display: true,
                            scaleLabel: {
                                display: false
                            }
                        }],
                        yAxes: [{
                            display: true,
                            scaleLabel: {
                                display: false
                            }
                        }]
                    }
                }

            });
        }
        if($('#downloadsMonth').is(':visible')) {
            var downloadsMonth_ctx = document.getElementById("downloadsMonth").getContext('2d');
            var downloadsMonth = new Chart(downloadsMonth_ctx, {
                type: 'bar',
                data: {
                    labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul'],
                    datasets: [
                        {
                            label: 'Data',
                            backgroundColor: '#289cea',
                            borderColor: '#289cea',
                            data: [-100, 65, 100, 90, 10, -20, -60],
                            borderWidth: 2
                        },
                        {
                            label: 'Data2',
                            backgroundColor: '#40bcbc',
                            borderColor: '#40bcbc',
                            data: [40, -80, -40, 40, -30, -40, 100],
                            borderWidth: 2,
                            borderDash: [5,5]
                        },
                        {
                            label: 'Data3',
                            backgroundColor: '#ff6384',
                            borderColor: '#ff6384',
                            data: [50, 90, 85, 30, 10, -90, -10],
                            borderWidth: 2,
                        }
                    ]
                },
                options: {
                    responsive: true,
                    legend: {
                        display: false
                    },
                    title: {
                        display: false
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false,
                    },
                    hover: {
                        mode: 'nearest',
                        intersect: true
                    },
                    scales: {
                        xAxes: [{
                            stacked: true,
                            scaleLabel: {
                                display: false
                            }
                        }],
                        yAxes: [{
                            stacked: true,
                            scaleLabel: {
                                display: false
                            }
                        }]
                    }
                }

            });
        }
        if($('#salesUploads').is(':visible')) {
            var salesUploads_ctx = document.getElementById("salesUploads").getContext('2d');
            var downlsalesUploadssalesUploadsadsMonth = new Chart(salesUploads_ctx, {
                type: 'doughnut',
                data: {
                    datasets: [{
                        data: [15,85,26,34,41,],
                        backgroundColor: ['#ff6384','#ff9f40','#ffcd56','#4bc0c0','#36a2eb',],
                        label: 'Dataset 1'
                    }],
                    labels: [
                        'Red',
                        'Orange',
                        'Yellow',
                        'Green',
                        'Blue'
                    ]
                },
                options: {
                    responsive: true,
                    legend: {
                        position:'right',
                        // labels:{
                        //     fontSize: 16,
                        //     fontColor: '#000000',
                        //     padding:30,
                        //     boxWidth:30
                        // }
                    },
                    title: {
                       display: false
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false,
                    },
                    hover: {
                        mode: 'nearest',
                        intersect: true
                    },

                }

            });
        }

    },
    multiselect: function () {

        $('.search-item-multiselect, .tickets-create-item-multiselect').select2({
            placeholder:'Select from list',
            maximumInputLength: 0,
            allowClear: true,
            closeOnSelect:false,
            dropdownAutoWidth:true,
            dropdownParent:$('.search-items, .tickets-create-items'),
            width: '100%'
        }).on('change',function () {
            var total = $(this).val().length;
            var hiddenItems = 0;
            $(this).siblings('.select2').find('.select2-selection__rendered').children('.select2-selection__choice').each(function () {
                if(($(this).position().left + $(this).width()) > ($(this).parent('ul').width() - 65) ){
                    $(this).addClass('hidden');
                }
            });

            $(this).siblings('.select2').find('.select2-selection__rendered').attr('data-total', total);

            hiddenItems =  $(this).siblings('.select2').find('.select2-selection__rendered').find('.hidden').length;

            if(hiddenItems > 0){
                $(this).siblings('.select2').find('.select2-selection__rendered').addClass('showAmmount');
            } else {
                $(this).siblings('.select2').find('.select2-selection__rendered').removeClass('showAmmount');
            }

        }).on('select2:opening select2:closing', function( event ) {
            var $searchfield = $(this).parent().find('.select2-search__field');
            $searchfield.prop('disabled', true);
        });
    },
    dataTables: function () {
        if ($('#results-table').is(':visible')){
            $('#results-table').DataTable({
                dom:'tp',
                aaSorting: [],
                searching: false,
                responsive: {
                    breakpoints: [
                        {name: 'desktop', width: 1250},
                        {name: 'tablet', width: 1249},
                        {name: 'mobile', width: 767},
                    ]
                },
                autoWidth: false,
                pageLength:10,
                pagingType:'simple_numbers',
                columns:[
                    {orderable:false},
                    null,
                    {
                        orderable:false,
                        checkboxes:{
                            selectRow:true,
                            selectAllRender: '<label class="results-table-label"><input type="checkbox" class="dt-checkboxes results-table-checkbox"><span class="results-table-checkbox-inner"></span></label>'
                        },
                        render: function(data, type, row, meta){
                            if(type === 'display'){
                                data = '<label class="results-table-label"><input type="checkbox" class="dt-checkboxes results-table-checkbox"><span class="results-table-checkbox-inner"></span></label>';
                            }

                            return data;
                        }
                    },
                    {orderable:false},
                    null,
                    null,
                    {orderable:false}
                ],
                select: {
                    style:'multi',
                    selector:'td:nth-child(3) label'
                },
                language: {
                    paginate: {
                        previous: "<",
                        next:">"
                    }
                }
            }).on('select deselect', function () {
                var selectedItems = $(this).find('tr.selected').length;
                if(selectedItems > 0){
                    $('.results-table-actions').show();
                } else {
                    $('.results-table-actions').hide();
                }
            });
        }
        if ($('#tickets-table').is(':visible')){
            $('#tickets-table').DataTable({
                dom:'tp',
                aaSorting: [],
                searching: false,
                responsive: {
                    breakpoints: [
                        {name: 'desktop', width: 1400},
                        {name: 'tablet', width: 1399},
                        {name: 'mobile', width: 767},
                    ]
                },
                autoWidth: false,
                pageLength:10,
                pagingType:'simple_numbers',
                columns:[
                    {orderable:false},
                    null,
                    null,
                    // null,
                    // null,
                    null,
                    null,
                    null,
                    null
                ],
                language: {
                    paginate: {
                        previous: "<",
                        next:">"
                    }
                }
            })
        }
        if ($('#commentaries-table').is(':visible')){
            $('#commentaries-table').DataTable({
                dom:'tp',
                aaSorting: [],
                searching: false,
                responsive: {
                    breakpoints: [
                        {name: 'desktop', width: 1400},
                        {name: 'tablet', width: 1399},
                        {name: 'mobile', width: 767},
                    ]
                },
                autoWidth: false,
                pageLength:10,
                pagingType:'simple_numbers',
                columns:[
                    {orderable:false},
                    null,
                    null,
                    {orderable:false},
                    null,
                ],
                language: {
                    paginate: {
                        previous: "<",
                        next:">"
                    }
                }
            })
        }
        if ($('#briefcase-table').is(':visible')){
            $('#briefcase-table').DataTable({
                dom:'tp',
                aaSorting: [],
                searching: false,
                responsive: {
                    breakpoints: [
                        {name: 'desktop', width: 1400},
                        {name: 'tablet', width: 1399},
                        {name: 'mobile', width: 767},
                    ]
                },
                autoWidth: false,
                pageLength:10,
                pagingType:'simple_numbers',
                columns:[
                    {orderable:false},
                    {orderable:false},
                    null,
                    null,
                    null,
                    {orderable:false},
                    {orderable:false},
                ],
                language: {
                    paginate: {
                        previous: "<",
                        next:">"
                    }
                }
            })
        }
        if ($('#briefcaseDetails-table').is(':visible')){
            $('#briefcaseDetails-table').DataTable({
                dom:'tp',
                aaSorting: [],
                searching: false,
                responsive: {
                    breakpoints: [
                        {name: 'desktop', width: 1400},
                        {name: 'tablet', width: 1399},
                        {name: 'mobile', width: 767},
                    ]
                },
                autoWidth: false,
                pageLength:10,
                pagingType:'simple_numbers',
                columns:[
                    {orderable:false},
                    null,
                    null,
                    null,
                    {orderable:false},
                ],
                language: {
                    paginate: {
                        previous: "<",
                        next:">"
                    }
                }
            })
        }

    },
    modal: function () {
        $(document).on('click', '.modalOpen', function (e) {
            e.preventDefault();
            var target = $(this).data('target');
            $(target).fadeIn();
        }).on('click', '.modal-overlay, .modal-close', function (e) {
            e.preventDefault();
            $('.modal').fadeOut();
        }).on('click', '.modal-list-startEdit', function (e) {
            e.preventDefault();
            var text = $(this).parents('.modal-list-item').find('.modal-list-item-text').text();
            $(this).parents('.modal-list-item').addClass('editing').find('.modal-list-item-edit-input').val(text);
        }).on('click', '.modal-list-item-endEdit', function (e) {
            e.preventDefault();
            var text = $(this).parents('.modal-list-item').find('.modal-list-item-edit-input').val();
            $(this).parents('.modal-list-item').removeClass('editing').find('.modal-list-item-text').text(text);
        }).on('click', '.modal-list-item-cancelEdit', function (e) {
            e.preventDefault();
            $(this).parents('.modal-list-item').removeClass('editing');
        });
    },
    calendar: function () {
        var events = [
            {
                title: 'All Day Event',
                start: '2018-12-01',
                className: 'fc-content__red'
            },
            {
                title: 'Long Event',
                start: '2018-12-07',
                end: '2018-12-10',
                className: 'fc-content__blue'
            },
            {
                id: 999,
                title: 'Repeating Event',
                start: '2018-12-09T16:00:00',
                className: 'fc-content__green'
            },
            {
                id: 999,
                title: 'Repeating Event',
                start: '2018-12-16T16:00:00',
                className: 'fc-content__red'
            },
            {
                title: 'Conference',
                start: '2018-12-11',
                end: '2018-12-13',
                className: 'fc-content__blue'
            },
            {
                title: 'Meeting',
                start: '2018-12-12T10:30:00',
                end: '2018-12-12T12:30:00',
                className: 'fc-content__green'
            },
            {
                title: 'Lunch',
                start: '2018-12-12T12:00:00',
                className: 'fc-content__red'
            },
            {
                title: 'Meeting',
                start: '2018-12-12T14:30:00',
                className: 'fc-content__blue'
            },
            {
                title: 'Happy Hour',
                start: '2018-12-12T17:30:00',
                className: 'fc-content__green'
            },
            {
                title: 'Dinner',
                start: '2018-12-12T20:00:00',
                className: 'fc-content__red'
            },
            {
                title: 'Birthday Party',
                start: '2018-12-13T07:00:00',
                className: 'fc-content__blue'
            },
            {
                title: 'Click for Google',
                url: 'http://google.com/',
                start: '2018-12-28',
                className: 'fc-content__green'
            }
        ];
        if ($(window).width() < 768){
            $('#calendar').fullCalendar({
                defaultView: 'agendaDay',
                contentHeight:'auto',
                header: {
                    left: 'prev',
                    center: 'title',
                    right: 'next',
                },
                slotLabelFormat: 'H:mm',
                defaultDate: new Date(),
                navLinks: false, // can click day/week names to navigate views
                editable: true,
                eventLimit: 2, // allow "more" link when too many events
                timeFormat: 'MM/DD/YYYY H:mm',
                displayEventTime: true,
                displayEventEnd: true,
                events: events
            });
        } else {
            $('#calendar').fullCalendar({
                buttonText: {
                    today: 'Today',
                    day: 'Day1',
                    agendaDay: 'Day',
                    week: 'Week1',
                    agendaWeek: 'Week',
                    month: 'Month'
                },
                header: {
                    left: 'prev next',
                    center: 'title',
                    right: 'agendaDay, agendaWeek, month, day, week',
                },
                slotLabelFormat: 'H:mm',
                defaultDate: new Date(),
                navLinks: true, // can click day/week names to navigate views
                editable: true,
                eventLimit: 2, // allow "more" link when too many events
                timeFormat: 'MM/DD/YYYY H:mm',
                displayEventTime: true,
                displayEventEnd: true,
                events: events
            });
        }
    },
    editor: function () {
        $('.editor').summernote({
            height: 150,
            disableResizeEditor: true,
            toolbar:[
                ['style', ['bold', 'italic', 'underline', 'clear','strikethrough']],
                ['para', ['ul', 'ol']],
                ['misc', ['fullscreen', 'codeview']],
            ]
        });
    },
    vectorMap: function () {
        if($('#geography').is(':visible')) {
            $('#geography').vectorMap({
                map: 'us_aea',
                backgroundColor: '#ffffff',
                enableZoom: true,
                showTooltip: true,
                regionStyle: {
                    initial: {
                        fill: '#85c5e3',
                        "fill-opacity": 1,
                        stroke: 'none',
                        "stroke-width": 0,
                        "stroke-opacity": 1
                    },
                    hover: {
                        "fill-opacity": 0.8,
                        cursor: 'pointer'
                    },
                    selected: {
                        fill: 'yellow'
                    },
                    selectedHover: {}
                }
            });
        }
    },
    notifications: function () {
        if(window.location.pathname.indexOf('/search.html')>=0){
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "positionClass": "toast-top-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            toastr["success"]("Some success", "SUCCESS");
            toastr["info"]("Some info", "INFO");
            toastr["warning"]("Some warning", "WARNING");
            toastr["error"]("Some error", "ERROR");
        }
    }
};


/*------------------
Preloader
-------------------*/

$(window).on('load', function() {
    $('.preloader').delay(700).fadeOut('50');
});

